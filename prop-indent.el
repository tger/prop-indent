;; -*- lexical-binding: t -*-
;;
;; Prior art: http://pastebin.com/fcveHWnc
;; and the other one..
;;
;; MS1 - simplest use case - newline and indent
;; MS3 whole buffer on load
;; Future: elastic tabs??

(defvar pi-debug t)

;; (defun column-position (row col)
;;   "Return position of column COL taking tabs into account."
;;   )

(defun resize-whitespace (indent &optional minimum)
  "Calc space-width using :align-to of character above current indent."
  (defun find-indent-posn (n)
    "Find indent position starting search at line N-1."
    ;; TODO really consider tabs
    (if (and (< (+ (line-beginning-position n) indent) (line-end-position n))
             (not (memq (char-after (+ (line-beginning-position n) indent)) '(?\t ?\s))))
        (posn-at-point (+ (line-beginning-position n) indent))
      (find-indent-posn (1- n))))
  (unless (or (zerop indent)
	      (= (line-number-at-pos) 1))
    ;; Attach property to whitespace to the left of indented pos
    (let* ((indent-posn (find-indent-posn 0))
           (indent-x (car (posn-x-y indent-posn))))
      (save-excursion
        ;; To support tabs, otherwise postion could be calculated from indent
        (back-to-indentation)
        (put-text-property (1- (point)) (point) 'display
                           `(space . (:align-to (,indent-x))))))))
  
(defun prop-indent-line ()
  (interactive)
  (indent-for-tab-command)
  (resize-whitespace (current-indentation)))

(defun prop-indent-region (start end)
  (interactive "r")
  (save-excursion
    (goto-char start)
    (while (< (point) end)
      (resize-whitespace (current-indentation))
      (beginning-of-line 2))))

(defun prop-indent-buffer ()
  (interactive)
  (prop-indent-region (point-min) (point-max)))

(define-minor-mode prop-indent-mode
  "Toggle proportional indentation mode. "
  :keymap (let ((map (make-sparse-keymap)))
            (define-key map (kbd "C-c C-i") 'prop-indent-line)
            map)
  ;(advice-add 'indent-to :after #'resize-whitespace) ; N.B: advising primitive
  (setq indent-tabs-mode nil)
  (prop-indent-buffer))

(provide 'prop-indent)
